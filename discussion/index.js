console.log("hello from JS"); //MESSAGE

//[SECTION] CONTROL STRUCTURES


//[SUB SECTION]: IF-ELSE STATEMENT

let numA = 3; //we will try to assess the value.

//If statement (Branch)
//The task of the if statement is to execute a procudure/action if the specified condition is "true".
if (numA <= 0) {
	//truthy branch
	//this block of code will run if the condition is MET.
	console.log('The condition was MET!'); 
}

//let name = 'Lorenzo'; //Allowed
//Maria => NOt allowed

let isLegal = false; 
//! -> NOT //false

//create a control structure that will allow the user to proceed if the value matches/passes the condition.
if (isLegal) {
	//if the passess the condition the "truthy" branch will run
	console.log('User can proceed!');
}

//[SUB SECTION] Else statement

// => This executed a statement if ALL other conditions are "FALSE" and/or has failed.

//lets create a control structure that will allow us to simulate a user login.


//prompt box -> prompt(): this will allow us to display a prompy dialog box which we can the user for an input.

//syntax: prompt(text/message[REQUIRED], placeholder/default text[OPTIONAL]);
//prompt("Please enter your first name:", "Martin"); 

//lets create a control structure that will allow us to check of the user is old enough to drink,
let age = 21; 

//alert() => display a message box to the user which would require their attention
// if (age >= 18) {
// 	// "truthy" brach
// 	alert("Your old enough to drink");
// } else {
// 	//"falsy" branch
// 	alert("Come back another day!");
// }


// lets create a control structure that will ask for the number of drinks that you will order. 

//ask the user how many drinks he wants
// let order = prompt('How many orders of drinks do you like?');

//convert the string data type into a number.
//parseInt() => will allow us to convert strings into integers

//create a logic that will make sure that the user's input is greater than 0

//Type coercion => conversion data type was converted to another data type.

//composite -> multiple data 
//primitive -> single data 

//1. if one of the operands in an object, it will be converted into a primitive data type (str, number, boolean).
//2. if atleast 1 operand is a string it will convert the other operand into a string as well. 
//3. if both numbers are numbers then an arithmetic operation will be executed.

//in JS there 3 ways to multiply a string. 
   //1. repeat() method => this will allow us to return a new string value that contains the number of copies of the string. 
      //syntax: str.repeat(value/number)

   //2. loops => for loop
   //3. loops => while loop method.

// x + y 
// if (order > 0) {
//   console.log(typeof order);
//   let cocktail = "🥤"; 
//   alert(cocktail.repeat(order));
//   //alert(order * "🥤"); 
//     //multiplication	
// } else {
// 	alert('The number should be above 0');
// }

//You want to create other predetermined conditions you can create a nested (multiple) if-else statement. 



//15 mins to formulate a logic that will allow us to multiply a string to a desired number.



//mini task

function vaccineChecker(){
	let vax = prompt('what brand is your vaccine?');
    vax = vax.toLowerCase();

    if (vax === 'pfizer' || vax === 'moderna' || vax === 'astrazenica' || vax === 'janssen') {
	alert(vax + ' Is allowed to travel');
    }  else {
    	alert('Sorry not permitted');
    }
}

// vaccineChecker();

//onclick =>is an example of a JS Event. this "event" in JS occurs when the user clicks on an element/component to trigger a certain procedure. 

//syntax: <element/component onclick="myScript/Function" >


function determineTyphoonIntensity(){

let windspeed = parseInt(prompt('Wind speed: '));
console.log(typeof windspeed);

if (windspeed <= 29) {
alert('Not a Typhoon yet!');
} else if (windspeed >= 30 && windspeed <= 61) {
alert('Tropical Depression Detected');
} else if (windspeed <= 88) {
	alert('Tropical storm detected!');
} else if (windspeed >= 89 && windspeed <= 117) {
	alert('Severe tropical storm');
}
else {
	alert('Typhoon Detected');
}

}

// conditional ternary

function ageChecker() {
	let age = parseInt (prompt('How old are you?'));

	(age >= 18) ? alert('Old enough to vote') : alert('Not yet old enough');

}


function determineComputerOwner() {
	let unit = prompt('What is the unit no. ?');
	let manggagamit;
	switch (unit) {

		case '1':
		alert("John Travolta");
		break;
		case '2':
		alert('Steve Jobs');
		break;
		case '3':
		alert('Sid Meier');
		break;
		case '4':
		alert('Onel de Guzman');
		break;
		case '6':
		alert('Christian Salvador');
		break;
		default: 
		manggagamit = 'Wala yan sa options na pagpipilian'
	}

		console.log(manggagamit);
}
// determineComputerOwner();


let dialog = "Drink your water bhie mimiyuhh says"

